//Example code: A simple server side code, which echos back the received message.
//Handle multiple socket connections with select and fd_set on Linux
#include <stdio.h>

#include <string.h> //strlen

#include <stdlib.h>

#include <errno.h>

#include <unistd.h> //close

#include <arpa/inet.h> //close

#include <sys/types.h>

#include <sys/socket.h>

#include <netinet/in.h>

#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros

#include <sqlite3.h>

# define TRUE 1

# define FALSE 0

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
   int i;
   for(i = 0; i<argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}

int main(int argc, char * argv[])
{
    int portno;
    /**********************************DATABASE***********************************/
    sqlite3 *db;
    char *query = NULL;
    sqlite3_stmt *stmt;
    int rc;
    char *sql;
    char *zErrMsg = 0;


rc = sqlite3_open("sensors.db", &db);

   if( rc ) {
      fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
      return(0);
   } else {
      fprintf(stderr, "Opened database successfully\n");
   }

/******************************************************************************/
    if (argc != 2) {
        fprintf(stderr, "usage: %s <port>\n", argv[0]);
        exit(1);
    }
    portno = atoi(argv[1]);

    int opt = TRUE;
    int master_socket, addrlen, new_socket, client_socket[2], max_clients = 2, activity, i, valread, sd;
    int max_sd;
    struct sockaddr_in address;

    char buffer[1025]; //data buffer of 1K

    //set of socket descriptors
    fd_set readfds;

    //a message
    char * message = "Values..:\n";

    //initialise all client_socket[] to 0 so not checked
    for (i = 0; i < max_clients; i++)
    {
        client_socket[i] = 0;
    }

    //create a master socket
    if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
		{
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // set master socket to allow multiple connections ,
    // this is just a good habit, it will work without this
    if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char * ) & opt,
            sizeof(opt)) < 0) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    //type of socket created
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons((unsigned short)portno);

    //bind the socket to localhost port 8888
		if( bind(master_socket,(struct sockaddr *)&address , sizeof(address)) < 0)
    {
        //print the error message
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");

    //try to specify maximum of 3 pending connections for the master socket
    if (listen(master_socket, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

		//listen(master_socket , 3);

    //accept the incoming connection
    addrlen = sizeof(address);
    puts("Waiting for connections ...");

    while (TRUE) {
        //clear the socket set
        FD_ZERO( & readfds);

        //add master socket to set
        FD_SET(master_socket, & readfds);
        max_sd = master_socket;

        //add child sockets to set
        for (i = 0; i < max_clients; i++)
				{
            //socket descriptor
            sd = client_socket[i];

            //if valid socket descriptor then add to read list
            if (sd > 0)
                FD_SET(sd, & readfds);

            //highest file descriptor number, need it for the select function
            if (sd > max_sd)
                max_sd = sd;
        }

        //wait for an activity on one of the sockets , timeout is NULL ,
        //so wait indefinitely
        activity = select(max_sd + 1, & readfds, NULL, NULL, NULL);

        // if ((activity < 0) && (errno != EINTR))
				//  {
        //     printf("select error");
        // 	}

        //If something happened on the master socket ,
        //then its an incoming connection
        if (FD_ISSET(master_socket, & readfds))
				 {
            if ((new_socket = accept(master_socket,(struct sockaddr * ) & address, (socklen_t * ) & addrlen)) < 0)
						{
                perror("accept");
                exit(EXIT_FAILURE);
            }

            //inform user of socket number - used in send and receive commands
            printf("New connection , socket fd is %d , ip is : %s , port : %d\n", new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port));

            //send new connection greeting message
            if (send(new_socket, message, strlen(message), 0) != strlen(message))
						  // if (write(new_socket, message, strlen(message)) < 0)
						 {
                perror("Not Send");
            	}

            puts("Welcome message sent successfully");

            //add new socket to array of sockets
            for (i = 0; i < max_clients; i++) {
                //if position is empty
                if (client_socket[i] == 0)
								{
                    client_socket[i] = new_socket;
                    printf("Adding to list of sockets as %d\n", i);

                    break;
                }
            }
        }

        //else its some IO operation on some other socket
        for (i = 0; i < max_clients; i++)
				{
            sd = client_socket[i];

//Returns a non-zero value if the file descriptor is set in the
// file descriptor set pointed to by fdset; otherwise returns 0.
            if (FD_ISSET(sd, & readfds))
						{
                //Check if it was for closing , and also read the
                //incoming message
                if ((valread = read(sd, buffer, 1024)) == 0)
								{
                    //Somebody disconnected , get his details and print
                    getpeername(sd, (struct sockaddr * ) & address, (socklen_t * ) & addrlen);
                    printf("Host disconnected , ip %s , port %d \n", inet_ntoa(address.sin_addr), ntohs(address.sin_port));

                    //Close the socket and mark as 0 in list for reuse
                    close(sd);
                    client_socket[i] = 0;
                }

                //Echo back the message that came in
                else {
                    //set the string terminating NULL byte on the end
                    //of the data read
                    buffer[valread] = '\0';
                    char str[100];
                    if (sd == client_socket[0])
                    {
                        strcpy(str,buffer);
                        printf("String from client_socket[0]%s",str );
                    }
                    else
                    {
                      printf("%c\n",buffer[1] );
                      switch (buffer[1])
                          {
                            case '1':
                            {
                              printf("RTC sensor\n");
                              printf("Configured Soon.....\n");
                              strcpy (buffer,"Configured Soon.....");
                              write(client_socket[1], buffer, strlen(str));
                              close(sd);
                              break;
                            }
                            case '2':
                            {
                              printf("Gyro sensor\n");
                              printf("Configured Soon.....\n");
                              strcpy (buffer,"Configured Soon.....");
                              write(client_socket[1], buffer, strlen(str));
                              close(sd);
                              break;
                            }
                            case '3':
                            {
                              printf("Temperature Sensor[Lm75]\n");
                              send(client_socket[1], str, strlen(str),0);
/******************************************DATABASE*************************************/
                              /* Create SQL statement */
                              sql = sqlite3_mprintf("INSERT INTO dumptemp (temperarute) "  \
                              "VALUES ('%q'); ",str);

                              /* Execute SQL statement */
                              rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

                              if( rc != SQLITE_OK ){
                              fprintf(stderr, "SQL error: %s\n", zErrMsg);
                              sqlite3_free(zErrMsg);
                              } else {
                              fprintf(stdout, "Records created successfully\n");
                              }
                              sqlite3_close(db);
                              // asprintf(&query, "insert into dumptemp (temperarute) values ('%s');", str);         /* 1 */
                              //
                              // sqlite3_prepare_v2(db, query, strlen(query), &stmt, NULL);                              /* 2 */
                              //
                              // rc = sqlite3_step(stmt);
                              // if (rc != SQLITE_DONE) {
                              //     printf("ERROR inserting data: %s\n", sqlite3_errmsg(db));
                              //     sqlite3_close(db);
                              // }
                              //
                              // sqlite3_finalize(stmt);
                              // free(query);                                                                            /* 3 */
/***************************************************************************************/
                              close(sd);
                              break;
                            }
                            case '4':
                            {
                              printf("ADC\n");
                              printf("Configured Soon.....\n");
                              strcpy (buffer,"Configured Soon.....");
                              write(client_socket[1], buffer, strlen(str));
                              close(sd);
                              break;
                            }
                            case '5':
                            {
                              printf("Moisture\n");
                              printf("Configured Soon.....\n");
                              strcpy (buffer,"Configured Soon.....");
                              write(client_socket[1], buffer, strlen(str));
                              close(sd);
                              break;
                            }
                          }
                    }
                }
            }
        }
    }

    return 0;
}
